import graphene
from graphene_django.types import DjangoObjectType
from django.db.models import Q

from .models import Book, Author, BookInstance


class BookType(DjangoObjectType):
    class Meta:
        model = Book


class BookInstanceType(DjangoObjectType):
    class Meta:
        model = BookInstance


class AuthorType(DjangoObjectType):
    books_custom = graphene.List(BookType, search=graphene.String())

    class Meta:
        model = Author

    def resolve_books_custom(parent, info, search=None):
        qs = Book.objects.filter(Q(author=parent))
        if search is not None:
            return qs.filter(Q(title__contains=search))

        return qs


class Query(object):
    author = graphene.List(AuthorType)
    hello = graphene.String()
    book = graphene.List(BookType, title=graphene.String())
    bookinstances = graphene.List(BookInstanceType)

    def resolve_hello(parent, info, **kwargs):
        return "World"

    def resolve_book(parent, info, title=None):
        if title is not None:
            return Book.objects.filter(title__contains=title)

        return Book.objects.all()

    def resolve_author(parent, info):
        return Author.objects.all()



    def resolve_bookinstances(parent, info):
        return BookInstance.objects.all()


class AuthorCreate(graphene.Mutation):
    author = graphene.Field(AuthorType)

    class Arguments:
        first_name = graphene.String(required=True)
        last_name = graphene.String(required=True)
        date_of_birth = graphene.Date(required=True)
        date_of_death = graphene.Date(required=False)

    def mutate(self, info, first_name,
               last_name,
               date_of_birth,
               date_of_death=None):
        author = Author(first_name=first_name,
                        last_name=last_name,
                        date_of_birth=date_of_birth,
                        date_of_death=date_of_death)
        author.save()

        return AuthorCreate(
            author=author
        )


class Mutation(graphene.ObjectType):
    author_create = AuthorCreate.Field()
